# load libraries

library(tidyverse)

# read in entries
klebsiella_metadata <- readr::read_csv("~/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Klebsiella_pneumoniae/microreact/microreact-project-GHRUNigeriaKpneumoniae-data.csv")


# colours
antibiotics_colours <- c("#CBD588", "#5F7FC7", "orange","#DA5724", "#508578", "#CD9BCD",
                         "#AD6F3B", "#673770","#D14285", "#652926", "#C84248", 
                         "#8569D5", "#5E738F","#D1A33D", "#8A7C64", "#599861")

# nice colours. colN=sample(color1, N). N = number of colours to generate
color1 <- grDevices::colors()[grep('gr(a|e)y', grDevices::colors(), invert = T)]
color7 <- sample(color1, 7)

klebsiella_metadata %>% filter(!is.na(O_locus_type)) %>% ggplot(aes(x = O_locus_type, fill = ST)) + 
  geom_bar() + ggthemes::theme_few() + scale_fill_manual(values = antibiotics_colours) + coord_flip() +
  ylab("Number of Isolates")

ggsave("~/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Klebsiella_pneumoniae/Pictures/Klebsiella_OserotypeST_figure.jpeg", last_plot(), device = "jpeg", width = 1800, height = 2000, units = 'px', dpi = 300)
