# klebsiella_project




```
cd existing_repo
git remote add origin https://gitlab.com/Ayorinde_Afo/klebsiella_project.git
git branch -M main
git push -uf origin main
```

## Name
Klebsiella project

## Description
This theoretically should point to the scripts I used for statistics and visualization for the klebsiella paper. Find a link to the paper [here](https://doi.org/10.1093/cid/ciab769).


## Authors and acknowledgment
Myself

## Project status
Project done.

